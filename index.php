<?php

include('config.php');

//Query : lecture du contenu de la table "fall"
$query = $pdo->prepare("SELECT * FROM fall ORDER BY date DESC");
//Query : exécution de la requete
$query->execute();

//Result : récupération des datas => ARRAY
$results = $query->fetchAll();

?>

<?php
if (isset($_GET['last'])) { //....index.php?last
    echo "Unité #".$results[0]["unit"]." : ".$results[0]["date"];
} elseif (isset($_GET['json'])) { //....index.php?json
    $datas = [];
    foreach ($results as $result) {
        $datas[$result['id']]['unit'] = $result['unit'];
        $datas[$result['id']]['date'] = $result['date'];
    }
    echo json_encode($datas);
} else { //....index.php ?>
    <table border="1" cellspacing="0" cellpadding="5">
        <thead>
        <tr>
            <th>#</th>
            <th>Bracelet</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($results as $result) { ?>
            <tr>
                <td><?php echo $result['id'] ?></td>
                <td><?php echo $result['unit'] ?></td>
                <td><?php echo $result['date'] ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>